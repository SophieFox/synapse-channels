from requests import get as http_get
import sqlite3
from db import db, db_session, Room

API_ROOM_LIST = r'https://matrix.org/_matrix/client/r0/publicRooms?start={next_batch}'

def get_room_list(next_batch=""):
    r = http_get(API_ROOM_LIST.format(next_batch=next_batch))
    r.raise_for_status()

    resp = r.json()

    return resp['chunk'], resp['next_batch']


@db_session
def main():
    rms, nb = get_room_list()
    print("Found {num} rooms, processing ...".format(num=len(rms)))

    """
    while len(rms) >= 1:
        allrooms += rms
        print("[nb: {}]".format(nb))
        rms, nb = get_room_list(next_batch=nb)
        print("Added {num} rooms ...".format(num=len(rms)))
    """
    
    def addtodb(r):
        fed = r['m.federate']
        del r['m.federate']
        r['m_federate'] = fed
        if 'aliases' in r:
            del r['aliases']
        Room(**r)
    for r in rms:
        try:
            addtodb(r)
        except Exception as ex:
            print(ex)


    #print("Got {num} rooms!".format(num=len(allrooms)))

if __name__ == '__main__':
    main()