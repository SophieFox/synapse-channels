from pony.orm import *


db = Database()
db.bind(provider='sqlite', filename='matrix-rooms.db', create_db=True)

class Room(db.Entity):
    #id = PrimaryKey(int, auto=True)
    room_id = PrimaryKey(str)
    num_joined_members = Required(int, index=True)
    m_federate = Required(bool)
    name = Optional(str, index=True)
    topic = Optional(str, index=True)
    canonical_alias = Optional(str, index=True)
    world_readable = Required(bool, index=True)
    guest_can_join = Required(bool, index=True)
    avatar_url = Optional(str)

db.generate_mapping(create_tables=True)

